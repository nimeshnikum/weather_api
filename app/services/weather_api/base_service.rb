module WeatherApi
  class BaseService
    PROVIDERS = [
      'WeatherApi::Providers::WeatherstackService',
      'WeatherApi::Providers::OpenweathermapService',
    ]
    @@current_provider = PROVIDERS[0]

    attr_reader :query, :response

    def initialize(city = 'Melbourne')
      @query = city
      @retries = 0
      @response = nil
    end

    def current
      puts "current provider: #{@@current_provider}"

      result = Rails.cache.fetch("cached_weather_data", expires_in: 3.seconds, force: false) do
        call_provider_api_with_fallback
      end

      # if successful response, override cache of last successful results
      # to be served as stale in future (upto 1 hour) in case of downtime
      Rails.cache.write('stale_weather_data', result, expires_in: 2.hours) if result

      # if result could not be fetched from any of providers, serve it from cache
      result || Rails.cache.read("stale_weather_data")
    end

    private

    # call provider api in failsafe mode. 
    # If one is down, it switches back to other one and sets it as first api to be called next time
    def call_provider_api_with_fallback
      while @retries < 2
        @response = call_provider_api
        unless @response
          @retries += 1
        else
          break
        end
      end
      @retries = 0
      @response
    end

    def call_provider_api
      begin
        @@current_provider.constantize.new(query).call
      rescue Net::ReadTimeout => e
        PROVIDERS[0], PROVIDERS[1] = PROVIDERS[1], PROVIDERS[0]
        @@current_provider = PROVIDERS[0]
        return nil
      end
    end

    def provider_api_response
      uri.query = URI.encode_www_form(params)
      request = Net::HTTP::Get.new(uri.to_s)

      res = Net::HTTP.start(uri.host, uri.port, read_timeout: 0.2) do |http|
        http.request(request)
      end

      api_response = JSON.parse(res.body, symbolize_names: true)
      build_payload(api_response)
    end
  end
end
