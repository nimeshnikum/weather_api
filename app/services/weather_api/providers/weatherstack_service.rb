require 'net/http'
require 'json'

module WeatherApi
  class Providers::WeatherstackService < BaseService
    def call
      provider_api_response
    end

    private

    def uri
      @uri ||= URI('http://api.weatherstack.com/current')
    end

    def params
      {
        access_key: ENV['WEATHERSTACK_ACCESS_KEY'],
        query: query
      }
    end

    def build_payload(api_response)
      {
        "wind_speed": api_response[:current][:wind_speed],
        "temperature_degrees": api_response[:current][:temperature]
      }
    end
  end
end
