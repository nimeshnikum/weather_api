require 'net/http'
require 'json'

module WeatherApi
  class Providers::OpenweathermapService < BaseService
    def call
      provider_api_response
    end

    private

    def uri
      @uri ||= URI('http://api.openweathermap.org/data/2.5/weather')
    end

    def params
      {
        appid: ENV['OPENWEATHERMAP_ACCESS_KEY'],
        q: query,
        units: 'metric' # to keep it consistent with other weather APIs
      }
    end

    def build_payload(api_response)
      {
        "wind_speed": api_response[:wind][:speed],
        "temperature_degrees": api_response[:main][:temp]
      }
    end
  end
end
