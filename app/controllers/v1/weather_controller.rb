class V1::WeatherController < ApplicationController
  def index
    weather_api_response = Weather::BaseService.new(params[:city]).current
    render json: weather_api_response
  end
end
