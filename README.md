# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version - 2.7.1

* OS environment - Mac OSX

* Database creation - None

# Application Setup

* Clone the repository: git clone https://nimeshnikum@bitbucket.org/nimeshnikum/weather_api.git

* Run `bundle install`

* Run application server - `rails s`

* In postman or using curl, call service - `http://localhost:3000/v1/weather?city=melbourne`

# What has been done so far

* Weather API service is written with possibility to easily extend for more providers with minimal code changes

* APIs are written in service-driven approach

* It uses Ruby's net/http class as there was just a get action for both APIs

# Enhancments possible/pending

* Other rest API clients like httparty or faraday can be used if there are more functions and error handling is required

* TDD : todo